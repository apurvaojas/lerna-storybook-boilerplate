const { aliases } = require('../configs/aliases.config');
const {sassRules} = require('../configs/rules.config');

module.exports = {
    webpackFinal: (config) => {
        config.module.rules.push(sassRules);
        return {
            ...config,
            resolve: {
                ...config.resolve,
                alias: {
                    ...config.resolve.alias,
                    ...aliases
                }
            }
        }
    },
    stories: ['../src/**/*.stories.[tj]s'],
    addons: [
        '@storybook/addon-actions/register',
        '@storybook/addon-knobs/register',
        '@storybook/addon-viewport/register',
        {
            name: '@storybook/addon-docs',
            options: {
                configureJSX: true,
                babelOptions: {},
                sourceLoaderOptions: null,
            },
        },
    ],
};