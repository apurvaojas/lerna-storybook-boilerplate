const autoprefixer = require('autoprefixer');
const sassRules = {
    exclude: /node_modules/,
    test: /\.(scss)$/,
    use: [
        'style-loader',
        'css-loader',
        {
            loader: 'postcss-loader',
            options: {
                plugins: [
                    autoprefixer({
                        grid: "autoplace",
                    }),
                ],
            },
        },
        'sass-loader',
    ],
};
const rules = [
    {
        ...sassRules
    },
    {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
            loader: "babel-loader",
            options: {
                presets: ['@babel/preset-env', '@babel/react']
            }
        }
    }
];

module.exports = {allRules: rules, sassRules};
